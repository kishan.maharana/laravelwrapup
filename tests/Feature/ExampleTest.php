<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;     

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->json('POST','/api/product',[
            'name'=>'kishankumar',
            'slug'=>'kishankumarmaharana',
            'price'=>202
        ]);
        $response->assertStatus(201);
        $this->assertDatabaseHas('products',[

            'name'=>'kishan',
            'slug'=>'kishankumar',
            'price'=>200

        ]);
    }
    public function testrouteTest()
    {
        $response = $this->get('/specific/14');
        $response->assertStatus(200);
    }
    public function testcheckTest()
    {
        $response=$this->get('/home');
        $response->assertStatus(302);
    }
    public function testCookiesTest()
    {
        // Testing for cookies
        $response = $this->withCookie('color', 'blue')->get('/');

        $response = $this->withCookies([
            'color' => 'blue',
            'name' => 'Taylor',
        ])->get('/');
        $response->assertStatus(200);
    }
    public function testupdateTest()
    {
        $response = $this->json('POST','/api/testupdate/5',[
            'name'=>'testupdate',
            'slug'=>'tupdate',
            'price'=>105
        ]);
        $response->assertStatus(200);
    }
    public function testdeleteTest()
    {
        $response=$this->json('get','/api/testdelete/5');
        $response->assertStatus(200);
    }
   
}
