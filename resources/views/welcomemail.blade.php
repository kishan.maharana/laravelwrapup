<!DOCTYPE html>
<html>
<head>
	<title>Your Detail</title>

	{{-- Using Bootstap CDN for Styling --}}

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">


	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>

{{-- PHP code for getting the current registered user --}}

<?php

use App\User;

$userdetail = User::latest()->first(); 

?>

<h3>Thank you for Registering {{$userdetail->name}}</h3>
<div class="card">
  <div class="card-header">
    Your Personal Detail
  </div>
  <div class="card-body">
    <blockquote class="blockquote mb-0">
      <p>Name :{{$userdetail->name}}</p>
      <p>Email :{{$userdetail->email}}</p>
      <p>Phone :{{$userdetail->phone}}</p>
      <p>Longitude :{{$userdetail->longitude}}</p>
      <p>lattitude :{{$userdetail->lattitude}}</p>
      <p>User Name :{{$userdetail->username}}</p>
    </blockquote>
  </div>
</div>



</body>
</html>