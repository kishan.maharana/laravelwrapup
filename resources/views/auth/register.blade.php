@extends('layouts.app')
@section('content')

@if(count($errors)>0)

@foreach($errors->all() as $error)

<p class="alert alert-danger">{{$error}}</p>

@endforeach

@endif


<form action="{{url('/cregister')}}" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
  <fieldset>
    <legend>Register</legend>
    <div class="form-group">
      <label for="exampleInputEmail1">Name</label>
      <input type="text" class="form-control" name="name" aria-describedby="emailHelp" placeholder="Enter Name">
      
  </div>
  
  <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
      
  </div>
   <div class="form-group">
      <label for="exampleInputEmail1">UserName</label>
      <input type="text" class="form-control" name="username" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Provide an ">
      
  </div>
   <div class="form-group">
      <label for="exampleInputEmail1">Phone</label>
      <input type="number" class="form-control" name="phone" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Phone">
      
  </div>
  <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
  </div>
  <div class="form-group">
      <label for="exampleInputPassword1"> Confirm Password</label>
      <input type="password" class="form-control" name="password_confirmation" placeholder="confirm Password">
  </div>
  <div id="map">
    
  </div>
   <div class="form-group">
      <label for="exampleInputPassword1">Longitude</label>
      <input type="text" class="form-control" name="long" id="longitude" placeholder="Longitude">
  </div>
   <div class="form-group">
      <label for="exampleInputPassword1"> Lattitude</label>
      <input type="text" class="form-control" name="lat" id="lattitude" placeholder="Lattitude">
  </div>
     <div class="form-group">
      <label for="exampleInputPassword1"> Profile Picture</label>
      <input type="file" class="form-control" name="image" placeholder="Upload Your Profile Picture">
  </div>
  <div class="form-group">
      
      <input type="submit" class="form-control btn btn-primary" >
  </div>
  
</form>


@endsection
@section('scripts')

 <script>
      function initMap() {
        var myLatlng = {lat: 28.4595, lng: 77.0266};

        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 8, center: myLatlng});

        // Create the initial InfoWindow.
        var infoWindow = new google.maps.InfoWindow(
            {content: 'Click the map to get Lat/Lng!', position: myLatlng});
        infoWindow.open(map);

        // Configure the click listener.
        map.addListener('click', function(mapsMouseEvent) {
          // Close the current InfoWindow.
          infoWindow.close();

          // Create a new InfoWindow.
          infoWindow = new google.maps.InfoWindow({position: mapsMouseEvent.latLng});
          infoWindow.setContent(mapsMouseEvent.latLng.toString());
          infoWindow.open(map);
          var longlat=mapsMouseEvent.latLng.toString(); //storing the longitude and lattitude in a variable
          longlat=longlat.split(","); //splitting the string into array to get longitude and lattitude separately
          var long=longlat[0]; //getting longittude
          long=long.slice(1,long.length); //slicing ( symbol from the string
          var lat=longlat[1];//getting Lattirude
          lat=lat.slice(0,lat.length-1);//slicing ) symbol from the string
          document.getElementById('longitude').value=long; //sending the value to the input field
          document.getElementById('lattitude').value=lat;//sending the value to the input field
        });
      }
    </script>
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLwluJqwWjZLR0L59g_OwolOEZ5myY0kE&callback=initMap">
    </script>

@endsection