@extends('layouts.app')

@section('content')


<table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Email</th>
      <th scope="col">User Name</th>
      <th scope="col">Phone</th>
      <th scope="col">Longitude</th>
      <th scope="col">Lattitude</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($alluser as $item)
    <tr>
      <th scope="row">{{$item->name}}</th>
      <td>{{$item->email}}</td>
      <td>{{$item->username}}</td>
      <td>{{$item->phone}}</td>
      <td>{{$item->longitude}}</td>
      <td>{{$item->lattitude}}</td>
    </tr>
    @endforeach
  </tbody>
</table>	


@endsection


