@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Your Detail') }}</div>

                <div class="card-body">
                    <img src="{{asset('public/profile/'.Auth::user()->profile)}}" alt="" style="height: 100px;width: 100px; border-radius: 5px;" class="mx-auto d-block">
                    <p>Name :{{Auth::user()->name}}</p>
                    <p class="badge badge-success">User Name :{{Auth::user()->username}}</p>
                    <p>Email :{{Auth::user()->email}}</p>
                    <p>Longitude :{{Auth::user()->longitude}}</p>
                    <p>Lattitude :{{Auth::user()->lattitude}}</p>
                    <div id="map">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
  function initMap() {
        var myLatlng = {lat: {{Auth::user()->longitude}}, lng: {{Auth::user()->lattitude}}};

        var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 8, center: myLatlng});
        map.addListener('click', function(mapsMouseEvent) {
          // Close the current InfoWindow.
          infoWindow.close();
          // Create a new InfoWindow.
          infoWindow = new google.maps.InfoWindow({position: mapsMouseEvent.latLng});
          infoWindow.setContent(mapsMouseEvent.latLng.toString());
          infoWindow.open(map);
      });
}
</script>
<script defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLwluJqwWjZLR0L59g_OwolOEZ5myY0kE&callback=initMap">
</script>


@endsection
