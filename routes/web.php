<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

//Route for veryifying user while logging in

Route::post('/clogin','UserController@loginVerify')->name('cloginverify');

//Route for Registering an User

Route::post('/cregister','UserController@registerVerify')->name('cregisterverify');

//Route for calling stored procedure for getting all the users data

Route::get('/getalluser','UserController@gettingAllDataUsingStoredProcedure');

//Route for getting specific data of a user using stored procedure

Route::get('/specific/{id}','UserController@specificUser');

Route::get('/test',function(){
	return 'true';
});

Route::get('/check',function(){
	return response()->json([],200);
});

Route::get('/checktest',function(){
	return response(['created'=>true]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('login/github', 'Auth\LoginController@redirectToProvider');
Route::get('login/github/callback', 'Auth\LoginController@handleProviderCallback');