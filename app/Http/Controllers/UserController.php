<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use App\Product;
use Illuminate\Support\Facades\Mail;
class UserController extends Controller
{
	public function loginVerify(Request $req)
	{

		//Function for verifying user while logging in

		$this->validate($req,[
			'email' => 'required|max:500',
			'password' => 'required',
		],[
			'email.required' => ' please provide your email',
			'password.required' => ' please provide your password',
		]);
		if(Auth::attempt(['email'=>$req->email,'password'=>$req->password])) //Validating use credentials
		{
			return view('home');
		}
		else
		{
			//if user entered wrong credentials then show him the following message
			return redirect(url('/login'))->with('errmsg','unable to login');
		}

	}
	public function registerVerify(Request $req)
	{
		//Function for registering and user
		//Validating the users input

		$this->validate($req,[
			'name' => 'required|min:2|max:255',
			'email' => 'required|email|unique:users|max:255',
			'password' => 'required|confirmed|max:30',
			'long' => 'required|min:2|max:255',
			'lat' => 'required|min:2|max:255',
			'phone'=>'required|min:10|max:10',
			'username'=>'required|min:5|max:20',
			'image'=>'required|max:5000'
		],[
			'name.required' => ' please provide your name',
			'name.min' => 'minimum 2 characters required',
			'email.required' => ' please provide your email',
			'email.unique' => ' This email is already exist',
			'password.required' => ' please provide your password',
			'password.confirmed' => 'password doesnot match with your confirm password',
			'phone.required'=>'Please provide your phone number',
			'phone.min'=>'phone number must be 10 Digits',
			'phone.max'=>'phone number must be 10 Digits',
			'username.required'=>'username is required',
			'username.min'=>'username atleast 5 characters',
			'username.max'=>'username atmost 20 characters',
			'image.required'=>'image field required',
		]);

		//code for storing the data into the database

		$data=new User;
		$data->name=$req->name;
		$data->email=$req->email;
		$data->phone=$req->phone;
		$data->username=$req->username;
		$data->longitude=$req->long;
		$data->lattitude=$req->lat;
		$data->password=bcrypt($req->password);
		if($req->hasfile('image'))
		{
			$file=$req->file('image');
			$extension=$file->getClientOriginalExtension();
			$filename=time().'.'.$extension;
			$file->move('public/profile/',$filename);
			$data->profile=$filename;
		}
		else
		{
			return $req;
			$data->profile='';
		}
		$data->save();

		//Settingup the credentials for sending mail

		$maildetail["email"]=$req->email;
		$maildetail["client_name"]=$req->name;
		$maildetail["subject"]='Welcome Mail';

		//Code for sending the mail to the corresponding user

		Mail::send('welcomemail', $maildetail, function($message)use($maildetail) {
			$message->to($maildetail["email"], $maildetail["client_name"])
			->subject($maildetail["subject"]);
		});
		return redirect(url('/login'))->with('registered','Registration Successfull');

	}
	public function gettingAllDataUsingStoredProcedure()
	{
		//function for getting all the users using stored procedure
		$alluser = DB::select('call alluser()');
		return view('storedprocedure.allusersp',compact('alluser'));
	}
	public function specificUser($id)
	{
		$alluser = DB::select('call userbyparam(?)',array($id));
		return $alluser;
	}
	public function check(Request $req)
	{
		
		$product = new Product;
		$product->name=$req->name;
		$product->slug=$req->slug;
		$product->price=$req->price;
		$product->save();
		return response()->json([],201);
	}
	public function testupdate(Request $req,$id)
	{
		$product=Product::find($id);
		$product->name = $req->name;
		$product->slug=$req->slug;
		$product->price = $req->price;
		$product->save();
		return response()->json(200);
	}
	public function testdelete($id)
	{
		Product::find($id)->delete();
		return response()->json(200);
	}
}