<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class consoleTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consoleTest';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name=$this->ask('what is your name ?');
        $language=$this->choice('what is your preffered language',[

            'php',
            'python',
            'laravel'
        ]);
        $this->line("your name is {$name} and you like {$language}");
    }
}
